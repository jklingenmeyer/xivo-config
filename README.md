XiVO confd
==========

Running unit tests
==================

Tox
---
* Install the xivo-ci package from deb http://mirror.xivo.solutions/debian xivo-dev-tools main
* Remove debian/xivo-config if you already have the package generated with debuild

```
ci-tox
```

Manual
--------

```
# create isolated environment
pip install virtualenv
virtualenv env
source env/bin/activate

# setup environment
pip install -r requirements.txt
pip install -r test-requirements.txt

# tests
nosetests
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING file for details.
