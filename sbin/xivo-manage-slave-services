#!/bin/sh

PATH=/bin:/usr/bin:/sbin:/usr/sbin

usage() {
    cat <<-EOF
	usage: $(basename $0) action
	    action: start or stop to enable/disable slave services
	EOF
}

set_berofos_to_slave_state() {
    xivo-berofos -q --syslog slave
}

set_berofos_to_master_state() {
    xivo-berofos -q --syslog master
}

start_dhcp() {
    # apply replicated configuration
    xivo-create-config
    xivo-update-config

    # fetch updated phone MAC prefixes
    dhcpd-update -dr

    service isc-dhcp-server start
    update-rc.d isc-dhcp-server defaults
}

stop_dhcp() {
    update-rc.d isc-dhcp-server remove
    service isc-dhcp-server stop
}

enable_service() {
    start_dhcp
    set_berofos_to_slave_state
    service consul start
    xivo-update-keys
    xivo-service enable
    xivo-service start
    xivo-agentd-cli -c 'relog all'
}

disable_service() {
    xivo-service stop xivo
    xivo-service disable
    service consul stop
    set_berofos_to_master_state
    stop_dhcp
    xivo-service start
}

case $1 in
    start) enable_service;;
    stop)  disable_service;;
    *) usage;;
esac
