; XIVO Dialplan
; Copyright (C) 2006-2015  Avencall
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License along
; with this program; if not, write to the Free Software Foundation, Inc.,
; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

[agentstaticlogin]
exten = s,1,NoOp()
same  =   n,Gosub(xivo-chk_feature_access,s,1)
same  =   n,Set(XIVO_SRCNUM=${IF(${EXISTS(${XIVO_SRCNUM})}?${XIVO_SRCNUM}:${CALLERID(num)})})
same  =   n,Set(XIVO_CONTEXT=${IF(${EXISTS(${XIVO_CONTEXT})}?${XIVO_CONTEXT}:${XIVO_BASE_CONTEXT})})
same  =   n,Gosub(xivo-pickup,0,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_get_options,${ARG1})
same  =   n,GotoIf($[${XIVO_AGENTEXISTS} != 1]?error_no_such_agent,1)
same  =   n,GosubIf($["${XIVO_AGENTPASSWD}" != ""]?authenticate,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_login,${XIVO_AGENTID},${XIVO_SRCNUM},${XIVO_CONTEXT})
same  =   n,Goto(status_${XIVO_AGENTSTATUS},1)
same  =   n,Hangup()

exten = error_no_such_agent,1,NoOp()
same  =   n,Playback(pbx-invalid)
same  =   n,Hangup()

exten = authenticate,1,NoOp()
same  =   n,Authenticate(${XIVO_AGENTPASSWD})
same  =   n,Return()

exten = status_logged,1,NoOp()
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogin,INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogin,INUSE,${XIVO_AGENTNUM})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogoff,NOT_INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogoff,NOT_INUSE,${XIVO_AGENTNUM})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogtoggle,INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogtoggle,INUSE,${XIVO_AGENTNUM})
same  =   n,Playback(agent-loginok)
same  =   n,Hangup()

exten = status_already_logged,1,NoOp()
same  =   n,Playback(agent-alreadylogged)
same  =   n,Hangup()

exten = status_already_in_use,1,NoOp()
same  =   n,Playback(pbx-invalid)
same  =   n,Hangup()


[agentstaticlogoff]
exten = s,1,NoOp()
same  =   n,Gosub(xivo-chk_feature_access,s,1)
same  =   n,Gosub(xivo-pickup,0,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_get_options,${ARG1})
same  =   n,GotoIf($[${XIVO_AGENTEXISTS} != 1]?error_no_such_agent,1)
same  =   n,GosubIf($["${XIVO_AGENTPASSWD}" != ""]?authenticate,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_logoff,${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogin,NOT_INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogin,NOT_INUSE,${XIVO_AGENTNUM})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogoff,INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogoff,INUSE,${XIVO_AGENTNUM})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogtoggle,NOT_INUSE,*${XIVO_AGENTID})
same  =   n,AGI(agi://${XIVO_AGID_IP}/phone_progfunckey_devstate,agentstaticlogtoggle,NOT_INUSE,${XIVO_AGENTNUM})
same  =   n,Playback(vm-goodbye)
same  =   n,Hangup()

exten = error_no_such_agent,1,NoOp()
same  =   n,Playback(pbx-invalid)
same  =   n,Hangup()

exten = authenticate,1,NoOp()
same  =   n,Authenticate(${XIVO_AGENTPASSWD})
same  =   n,Return()


[agentstaticlogtoggle]
exten = s,1,NoOp()
same  =   n,Gosub(xivo-chk_feature_access,s,1)
same  =   n,Gosub(xivo-pickup,0,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_get_options,${ARG1})
same  =   n,GotoIf($[${XIVO_AGENTEXISTS} != 1]?error_no_such_agent,1)
same  =   n,AGI(agi://${XIVO_AGID_IP}/agent_get_status,${XIVO_AGENTID})
same  =   n,Goto(login_status_${XIVO_AGENT_LOGIN_STATUS},1)

exten = error_no_such_agent,1,NoOp()
same  =   n,Playback(pbx-invalid)
same  =   n,Hangup()

exten = login_status_logged_out,1,NoOp()
same  =   n,Gosub(agentstaticlogin,s,1(${XIVO_AGENTNUM}))

exten = login_status_logged_in,1,NoOp()
same  =   n,Gosub(agentstaticlogoff,s,1(${XIVO_AGENTNUM}))


[agentcallback]
exten = _id-.,1,NoOp()
same  =   n,Set(XIVO_AGENT_ID=${EXTEN:3})
same  =   n,AGI(agi://${XIVO_AGID_IP}/incoming_agent_set_features,${XIVO_AGENT_ID})
same  =   n,UserEvent(Agent,CHANNEL: ${CHANNEL},XIVO_AGENT_ID: ${XIVO_AGENT_ID})
same  =   n,Set(XIVO_PRESUBR_GLOBAL_NAME=AGENT)
same  =   n,GoSub(xivo-subroutine,s,1(${XIVO_AGENTPREPROCESS_SUBROUTINE}))
same  =   n,Gosub(xivo-global-subroutine,s,1)
same  =   n,Dial(${XIVO_AGENT_INTERFACE},,${XIVO_CALLOPTIONS}${XIVO_QUEUECALLOPTIONS})
same  =   n,Hangup()


; ARG1 : dialed exten
[agentfunckeypause]
exten = s,1,NoOp(Pause/Unpause request)
same  =   n,Set(XIVO_AGENT_FKEY=${ARG1})
same  =   n,Set(XIVO_AGENT_EXTENSION=${STRREPLACE(XIVO_AGENT_FKEY,***34,)})
same  =   n,NoOp(Pause/Unpause request for agent at extension ${XIVO_AGENT_EXTENSION} )
same  =   n,Answer()
same  =   n,AGI(/var/lib/asterisk/agi-bin/xuc-request.py,togglePause,${XIVO_AGENT_EXTENSION})
same  =   n,NoOp(${XIVO_XUCWS_CONTENT})
same  =   n,GotoIf($["${XIVO_XUCWS_STATUS_CODE}" == "200"]?success:failed)
same  =   n(failed),Playback(error-sorry)
same  =   n(success),Hangup()


; ARG1 : dialed exten
[agentfunckeylogin]
exten = s,1,NoOp(Login/logout request)
same  =   n,Set(XIVO_AGENT_FKEY=${ARG1})
same  =   n,Set(XIVO_AGENT_EXTENSION=${STRREPLACE(XIVO_AGENT_FKEY,***30,)})
same  =   n,GosubIf($[${REGEX("[0-9]{1,}\\*[0-9]{1,}" ${XIVO_AGENT_EXTENSION})}]?get_agentnum_from_fkey,1:)
same  =   n,Set(XIVO_AGENT_STATUS=${DEVICE_STATE(Custom:${XIVO_AGENT_FKEY})})
same  =   n,GotoIf($["${XIVO_AGENT_STATUS}" == "INUSE"]?logout,1:)
same  =   n,GotoIf($["${XIVO_AGENT_NUMBER}" == ""]?ask_agentnum,1:)
same  =   n,Goto(login,1)

exten = get_agentnum_from_fkey,1,NoOp(Get agent number from fkey)
same  =   n,Set(XIVO_AGENT_NUMBER=${CUT(XIVO_AGENT_EXTENSION,*,2)})
same  =   n,Set(XIVO_AGENT_EXTENSION=${CUT(XIVO_AGENT_EXTENSION,*,1)})
same  =   n,Return()

exten = ask_agentnum,1,NoOp(Ask agent number)
same  =   n,Read(XIVO_AGENT_NUMBER,agent-user,,1,3)
same  =   n,GotoIf($["${READSTATUS}" != "OK"]?:login,1)
same  =   n,Playback(error-sorry)
same  =   n,Hangup()

exten = login,1,NoOp(Login agent)
same  =   n,AGI(/var/lib/asterisk/agi-bin/xuc-request.py,agentLogin,${XIVO_AGENT_EXTENSION},${XIVO_AGENT_NUMBER})
same  =   n,NoOp(${XIVO_XUCWS_CONTENT})
same  =   n,GotoIf($["${XIVO_XUCWS_STATUS_CODE}" == "200"]?loginsuccess:loginfailed)
same  =   n(loginfailed),GotoIf($["${XIVO_XUCWS_CONTENT:-14:14}" == "does not exist"]?notfound)
same  =   n,Playback(error-sorry)
same  =   n,Hangup()
same  =   n(notfound),Playback(agent-incorrect)
same  =   n,Goto(ask_agentnum,1)
same  =   n(loginsuccess),Playback(agent-loginok)
same  =   n,Hangup()

exten = logout,1,NoOp(Logout agent)
same  =   n,AGI(/var/lib/asterisk/agi-bin/xuc-request.py,agentLogout,${XIVO_AGENT_EXTENSION})
same  =   n,NoOp(${XIVO_XUCWS_CONTENT})
same  =   n,GotoIf($["${XIVO_XUCWS_STATUS_CODE}" == "200"]?logoutsuccess:logoutfailed)
same  =   n(logoutfailed),Playback(error-sorry)
same  =   n,Hangup()
same  =   n(logoutsuccess),Playback(agent-loggedoff)
same  =   n,Hangup()


