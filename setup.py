#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup
import fnmatch
import os


def is_package(path):
    is_svn_dir = fnmatch.fnmatch(path, '*/.svn*')
    is_test_module = fnmatch.fnmatch(path, '*tests')
    return not (is_svn_dir or is_test_module)

packages = [p for p, _, _ in os.walk('xivo_config') if is_package(p)]


setup(
    name='xivo-config',
    version='0.1',
    description='XIVO Config',
    author='Avencall',
    author_email='dev@avencall.com',
    url='http://www.xivo.solutions/',
    license='GPLv3',
    packages=packages,
    scripts=['sbin/xivo-berofos', 'sbin/xivo-create-config',  'sbin/xivo-strip-queue-info',
             'sbin/xivo-update-odbcinst-config'],
    data_files=[('/usr/share/xivo-config/x509/', ['x509/openssl-x509.conf'])],
)
